from __future__ import print_function
from collections import defaultdict
from bs4 import BeautifulSoup
from logging.config import fileConfig

import logging
import argparse
import csv
import json
import os

cur_dir = os.path.dirname(os.path.realpath(__file__))


class App():

    def __init__(self, **entries):

        self.id = entries['App Id']
        self.title = entries['Title']
        self.source = entries['Source']
        self.description = entries['App Description']
        self.datasets = []

    def pop_datasets(self, data):
        """ Update App Datasets """

        sets = defaultdict(list)
        for set in data:
            sets[set['Dataset']].append(set)

        self.datasets = [DataSchema(name, columns)
                         for (name, columns) in sets.items()]

    def update_datasets(self, columns):
        """ Update dataset schema descriptions """

        [ds.add_column_descriptions(columns) for ds in self.datasets]

    def build_template(self, tpl):
        """ Build template """

        self.guide = tpl
        self.guide.find('h2').string = self.title
        self.guide.find(id="title").string = self.title
        self.guide.find(
            id='custom-data-container').replaceWith(self._pop_custom())
        self.guide = str(self.guide).replace('\n', '')

    def _pop_custom(self):
        """ Fill Custom Container """

        custom = self.guide.find(id='custom-data-container')

        for ds in self.datasets:
            table = self.guide.new_tag('table')
            table['id'] = ds.name.replace(" ", "-")
            tbody = self.guide.new_tag('tbody')

            # Dataset Detail
            detail = BeautifulSoup(
                open(os.path.join(cur_dir, 'templates/detail.tpl.html')),
                'html.parser')

            detail.select('td.value')[0].string=ds.name
            detail.select('td.value')[1].string=ds.description
            tbody.append(detail)

            for col in ds.schema:
                row=BeautifulSoup(''
                    '<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td>'
                    '<td colspan="2">{}</td></tr>'.format(col['name'],
                        col['dataType'], ds.name, self.source, col['desc']),
                    'html.parser')
                tbody.append(row)

            table.append(tbody)
            custom.append(table)
            custom.append(BeautifulSoup('<p>&nbsp;</p>', 'html.parser'))

        return custom


class DataSchema():

    def __init__(self, name, columns):
        """ Build schema from columns """

        self.name=name
        self.description='unknown'
        self.schema=[dict(name=x['Column Name'],
                          dataType=x['DataType']) for x in columns]

    def add_column_descriptions(self, columns):
        """ Update column descriptions """

        for index, column in enumerate(self.schema):
            matches=[x for x in columns
                       if x.get('dataType').lower() == column.get('dataType').lower()
                       and x.get('name').lower() == column.get('name').lower()]

            if matches:
                self.schema[index]['desc']=matches[0].get('desc')
            else:
                self.schema[index]['desc']='unknown'


def get_file_content(path):
    """ Read file into memory """

    with open(path, 'r') as file:
        reader=csv.DictReader(file)
        return [x for x in reader]


def get_apps(path):
    """ Read app list from file """

    apps=get_file_content(path)
    return [App(**app) for app in apps]


def get_schemas(path):
    """ Read app datasets from file """

    sets=get_file_content(path)
    groups=defaultdict(list)
    [groups[set['App Id']].append(set) for set in sets]
    return groups


def get_columns(path):
    """ Read unique columns from file """

    columns=get_file_content(path)
    return [dict(name=x['Column Name'], dataType=x['DataType'],
                 desc=x['Description']) for x in columns]


def prep_output(apps, data, columns, logger):
    """ Build result html """

    soup=BeautifulSoup(
        open(os.path.join(cur_dir, 'templates/output.tpl.html')),
        "html.parser")

    for app in apps:
        logger.info('Processing App {}'.format(app.id))
        tpl=BeautifulSoup(open(args.tpl), "html.parser")
        app.pop_datasets(data[app.id])
        app.update_datasets(columns)
        app.build_template(tpl)

        guide_str=app.guide.replace(
            '<', '&#60;').replace(
            '>', '&#62;').replace(
            '"', '&#34;')

        row=BeautifulSoup(''
            '<tr><td>{}</td><td><input class="form-control" id="guide{}" '
            'value="{}"></input></td><td><button class="btn btn-info" '
            'data-clipboard-target="#guide{}"><i class="fa fa-clipboard"></i>'
            '</button></td></tr>'
            .format(app.id, app.id, guide_str, app.id), 'html.parser')

        soup.select('tbody')[0].append(row)

    return soup


def write_to_file(data):
    if not os.path.exists('dist'):
        os.makedirs('dist')

    results=open('dist/guides.html', 'w+')
    results.write(data)


def build_args():
    """ Helper function """

    tpl_path=os.path.join(cur_dir, 'templates/guide.tpl.html')

    parser=argparse.ArgumentParser("App Sherpa v0.0.1")
    parser.add_argument('--apps', required=True)
    parser.add_argument('--data', required=True)
    parser.add_argument('--desc', required=True)
    parser.add_argument('--tpl', default=tpl_path)

    return parser.parse_args()


if __name__ == '__main__':

    fileConfig('log_config.ini')
    logger=logging.getLogger(__name__)

    args=build_args()

    apps=get_apps(args.apps)
    logger.info('Apps ready...')

    data=get_schemas(args.data)
    logger.info('Datasets ready...')

    columns=get_columns(args.desc)
    logger.info('Columns ready...')

    soup=prep_output(apps, data, columns, logger)
    write_to_file(str(soup))

    logger.info('You\'re Done! {} Total Guides Created.'.format(len(apps)))
