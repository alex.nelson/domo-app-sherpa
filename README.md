# App Sherpa v.0.0.1

### Download

Option 1: Clone Repo

`git clone https://github.com/walexnelson/app-sherpa.git`

Option 2: Download [Zip](https://github.com/walexnelson/app-sherpa/archive/master.zip)

### Install dependencies

cd into the root directory via terminal

`pip install -r requirements.txt`

### Usage

cd into the root directory via terminal

##### Display all required paramaters and description

`python sherpa.py --help`

##### Example

`python sherpa.py --apps docs/master_list.csv --data docs/column_list.csv --desc docs/unique_columns.csv`

##### Example with Custom Template

`python sherpa.py --tpl /path/to/template.html --apps docs/master_list.csv --data docs/column_list.csv --desc docs/unique_columns.csv`
